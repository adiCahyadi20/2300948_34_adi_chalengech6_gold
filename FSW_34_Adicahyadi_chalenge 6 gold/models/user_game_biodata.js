"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User_game_biodata.hasOne(models.User_game_history, {
        foreignKey: "bio_id",
      });

      User_game_biodata.belongsTo(models.User_game, {
        foreignKey: "user_game_Id",
      });
      // define association here
    }
  }
  User_game_biodata.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      age: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      date_of_birth: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "user_game_biodata",
      modelName: "User_game_biodata",
    }
  );
  return User_game_biodata;
};
