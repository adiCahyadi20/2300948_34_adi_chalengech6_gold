"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User_game_history.belongsTo(models.User_game, {
        foreignKey: "user_game_Id",
      });

      User_game_history.belongsTo(models.User_game_biodata, {
        foreignKey: "bio_Id",
      });

      // define association here
    }
  }
  User_game_history.init(
    {
      play_time: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      total_match: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      win: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      lose: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "user_game_history",
      modelName: "User_game_history",
    }
  );
  return User_game_history;
};
