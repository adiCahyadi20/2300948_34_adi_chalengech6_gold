"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User_game.hasMany(models.User_game_history, {
        foreignKey: "user_game_Id",
      });

      User_game.hasMany(models.User_game_biodata, {
        foreignKey: "user_game_Id",
      });
      // define association here
    }
  }
  User_game.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "user_game",
      modelName: "User_game",
    }
  );
  return User_game;
};
