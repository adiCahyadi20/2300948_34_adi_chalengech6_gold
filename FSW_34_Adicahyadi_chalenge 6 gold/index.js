const express = require("express");
const bodyParser = require("body-parser");
const { sequelize, User_game, User_biodata, User_game_history } = require("./models");

const app = express();
let user = require("./db/login.json");

app.use(bodyParser.urlencoded({ extend: false }));
app.use(bodyParser.json());

app.use(express.static("public"));
app.use(express.json());
app.set("view engine", "ejs");

// route log in

app.get("/api/v1/login", function (req, res) {
  res.render("login");
});

app.post("/api/v1/login", function (req, res) {
  let username = req.body.username;
  let password = req.body.password;

  if (username == user.username && password == user.password) {
    res.redirect(301, "/dashboard");
  } else {
    res.end("invalid username or password");
  }
});

// route

app.get("/", function (req, res) {
  res.render("index");
});

app.get("/api/v1/about", function (req, res) {
  res.render("about");
});

app.get("/api/v1/contact", function (req, res) {
  res.render("contact");
});

app.get("/greet", function (req, res) {
  res.render("greet");
});

app.get("/api/v1/sign-up", function (req, res) {
  res.render("sign-up");
});

app.get("/api/v1/work", function (req, res) {
  res.render("work");
});

app.get("/game", function (req, res) {
  res.render("game");
});

//get dashboard
app.get("/dashboard", function (req, res) {
  res.render("dashboard");
});

// get user
app.get("/user", (req, res) => {
  User_game.findAll().then((users) => {
    res.render("user_game", {
      users,
    });
  });
});

// get create
app.get("/create", (req, res) => {
  res.render("create_user");
});

// post create user game
app.post("/create", (req, res) => {
  if ((!req.body.username, req.body.email, req.body.password)) {
    User_game.create({
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    }).then((users) => {
      res.redirect("/user");
    });

    return;
  }
  res.status(400).send({ message: "content can not be empty" });
});

// delete user game
app.get("/delete/:id", (req, res) => {
  User_game.destroy({
    where: { id: req.params.id },
  }).then((users) => {
    res.redirect("/user");
  });
});

// app.get("/user-history", (req, res) => {
//   User_game_history.findAll().then((user) => {
//     res.render("user_game_history", {
//       user,
//     });
//   });
// });

// app.get("/user-game-history", async (req, res) => {
//   try {
//     const users = await User_game_history.findAll()

//     return res.render("user_history");
//   } catch (err) {
//     console.log(err);
//     return res.status(500).json({ error: "Something went wrong!" });
//   }
// });

// app.get("/user-game", function (req, res) {
//   res.render("user_game");
// });
app.get("/user-game-biodata", function (req, res) {
  res.render("user_game_biodata");
});
app.get("/user-history", function (req, res) {
  res.render("user_game_history");
});

app.listen({ port: 3000 }, async () => {
  console.log("Server up on htpp://localhost:3000");
  await sequelize.authenticate();

  console.log("Database Connected!");
});
