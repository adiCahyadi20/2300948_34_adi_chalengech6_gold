"use strict";
/** @type {import('datatypes-cli').Migration} */
module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable("user_game_history", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      play_time: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      total_match: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      win: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      lose: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },

      user_game_Id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });
  },
  async down(queryInterface, DataTypes) {
    await queryInterface.dropTable("User_game_history");
  },
};
