class GameStart {
  // add proprety yang di perlukan
  constructor() {
    this.playerName = "player 1";
    this.comName = "com";
    this.comOptions = ["batu", "gunting", "kertas"];
    this.userOptions = document.querySelectorAll(".user-input");
    this.refresh = document.querySelector(".icon-refresh");
  }

  // membuat method random untuk pilahan com
  comBrain() {
    let index = this.comOptions[Math.floor(Math.random() * this.comOptions.length)];
    return index;
  }

  // menambahkan logic untuk hasil permainan
  result(com, player) {
    let resultLogic = {
      batukertas: "COM",
      batugunting: "Player 1",
      kertasbatu: "PLAYER 1",
      kertasgunting: "Player 1",
      kertasgunting: "COM",
      guntingbatu: "COM",

      guntingkertas: "COM",
      guntingkertas: "Player 1",
    };
    let resultValue = resultLogic[player + com];
    let result = com === player ? "DRAW" : `${resultValue} Win`;
    return result;
  }
  //  untuk menampilkan hasil pilihan com
  comRespon(comInput) {
    const removeRespon = document.querySelectorAll(".com");
    removeRespon.forEach((element) => {
      element.classList.remove("box-active");
    });

    const addRespon = document.querySelector(".com-" + comInput);
    addRespon.classList.add("box-active");
  }

  resultDisplay(com, player) {
    // menampilkan hasil permainan
    const resultDisplay = document.querySelector(".text-result");
    resultDisplay.classList.add("result", "box");
    resultDisplay.textContent = start.result(com, player);
  }

  // menambahkan method untuk mengambil input user

  userInput() {
    // permainan akan di mulai saat user mengklik pilihan
    this.userOptions.forEach(function (e) {
      e.addEventListener("click", function () {
        e.classList.add("box-active");
        const userInput = e.id;
        const comInput = start.comBrain();

        start.comRespon(comInput);
        start.resultDisplay(comInput, userInput);
        // console log hasil permainan
        let hasil = start.result(comInput, userInput);
        console.log(hasil);
        console.log(`${start.comName} memilih : ${comInput}`);
        console.log(`${start.playerName} memilih : ${userInput}`);
      });
    });
  }

  // menambahkan method untuk mereset semua elment

  reload() {
    this.refresh.addEventListener("click", function () {
      //  melakukan reset pada semua elment class yang di tambahkan

      const removeRespon = document.querySelectorAll(".com");
      removeRespon.forEach((element) => {
        element.classList.remove("box-active");
      });
      const remove = document.querySelectorAll(".user-input");
      remove.forEach((element) => {
        element.classList.remove("box-active");
      });

      const resultDisplay = document.querySelector(".text-result");
      resultDisplay.classList.add("text-vs");
      resultDisplay.classList.remove("result", "box");
      resultDisplay.innerHTML = "VS";
    });
  }
}

const start = new GameStart();
start.userInput();
start.reload();

// terima kasih kak fitria sudah meluangkan waktunya untuk meriview
// mohon maaf apabila masih banyak kekurangan
// semangat :)
